import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPoiPage } from './add-poi';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';
@NgModule({
  declarations: [
    AddPoiPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPoiPage),
  ],
  providers: [],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AddPoiPageModule { }
