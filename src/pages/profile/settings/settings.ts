import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController, ToastController } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {
  isChecked: boolean;
  notif: string;
  isBooleanforLive: boolean;
  isBooleanforDash: boolean;
  fuels: any[] = [];
  maps: string[] = [];
  selectedMapKey: string;
  mapKey: any;
  fuelKey: string;
  islogin: any;

  constructor(
    public events: Events,
    private tts: TextToSpeech,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public apiCall: ApiServiceProvider
    ) {
    this.fuels = ['LITRE', 'PERCENTAGE'];
    this.maps = ['Normal', 'Terrain', 'Hybrid', 'Satellite'];
    if (localStorage.getItem("notifValue") != null) {
      this.notif = localStorage.getItem("notifValue");
      if (this.notif == 'true') {
        this.isChecked = true;
      } else {
        this.isChecked = false;
      }
    }
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));

    if (this.islogin.fuel_unit === 'LITRE') {
      this.fuelKey = 'LITRE';
    } else {
      this.fuelKey = 'PERCENTAGE';
    }
    if (localStorage.getItem('MAP_KEY') != null) {
      this.selectedMapKey = localStorage.getItem('MAP_KEY');
    }
  }
  ngOnInit() {
    if (localStorage.getItem("SCREEN") != null) {
      if (localStorage.getItem("SCREEN") == 'live') {
        this.isBooleanforLive = true;
        this.isBooleanforDash = false;
      } else {
        if (localStorage.getItem("SCREEN") == 'dashboard') {
          this.isBooleanforDash = true;
          this.isBooleanforLive = false;
        }
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }
  setNotif(notif) {
    console.log(notif);
    this.events.publish('notif:updated', notif);
    this.isChecked = notif;
    if (notif === true) {
      this.tts.speak('You have succesfully enabled voice notifications')
        .then(() => console.log('Success'))
        .catch((reason: any) => console.log(reason));
    }
  }

  rootpage() {
    // let alert = this.alertCtrl.create({
    //   title: 'Alert',
    //   subTitle: 'Choose default screen',

    // })
    let alert = this.alertCtrl.create();
    // alert.setTitle('Alert');
    alert.setSubTitle('Choose default screen');
    alert.addInput({
      type: 'radio',
      label: 'Dashboard',
      value: 'dashboard',
      checked: this.isBooleanforDash
    });

    alert.addInput({
      type: 'radio',
      label: 'Live Tracking',
      value: 'live',
      checked: this.isBooleanforLive
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        console.log(data)
        localStorage.setItem("SCREEN", data);
        const toast = this.toastCtrl.create({
          message: 'Default page set to ' + data + ' page',
          duration: 2000
        });
        toast.present();
      }
    });
    alert.present();
  }

  onChangeMap(key) {
    console.log("map key changed: ", key)
    localStorage.setItem("MAP_KEY", key);
  }

  onChangeFuel(key) {
    console.log("key changed: ", key)
    const newContact = {
      fname: this.islogin.fn,
      lname: this.islogin.ln,
      org: this.islogin._orgName,
      uid: this.islogin._id,
      fuel_unit: key
    }

    var _baseURl = this.apiCall.mainUrl + "users/Account_Edit";
    this.apiCall.urlpasseswithdata(_baseURl, newContact)
      .subscribe(data => {
        console.log("got response data: ", data)
        var logindetails = JSON.parse(JSON.stringify(data));
        var userDetails = window.atob(logindetails.token.split('.')[1]);
        var details = JSON.parse(userDetails);
        localStorage.setItem('details', JSON.stringify(details));
      })
  }

}
